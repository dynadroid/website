#!/bin/sh

echo running cleanup scripts
git_repo=dynadroid/website

git rm $CI_PROJECT_DIR/static/.well-known/acme-challenge/$CERTBOT_TOKEN
git commit -m "GitLab runner - Removed certbot challenge file"
git push https://$GITLAB_USER_LOGIN:$PAT@gitlab.com/$git_repo.git HEAD:master
