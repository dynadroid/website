
#!/bin/sh

domain=dynadroid.nl

end_epoch=$(date -d "$(echo | openssl s_client -connect $domain:443 -servername $domain 2>/dev/null | openssl x509 -enddate -noout | cut -d'=' -f2)" "+%s")
current_epoch=$(date "+%s")
renew_days_threshold=30
days_diff=$((($end_epoch - $current_epoch) / 60 / 60 / 24))

if [ $days_diff -lt $renew_days_threshold ]; then
  echo "Certificate is $days_diff days old, renewing now."
  certbot certonly --manual --non-interactive --agree-tos --manual-public-ip-logging-ok --preferred-challenges=http -m $GITLAB_USER_EMAIL --manual-auth-hook $CI_PROJECT_DIR/scripts/letsencrypt_authenticator.sh --manual-cleanup-hook $CI_PROJECT_DIR/scripts/letsencrypt_cleanup.sh -d $domain -d www.$domain
  echo "Certbot finished. Updating GitLab Pages domains."
  curl --request PUT --header "PRIVATE-TOKEN: $PAT" --form "certificate=@/etc/letsencrypt/live/$domain/fullchain.pem" --form "key=@/etc/letsencrypt/live/$domain/privkey.pem" https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/pages/domains/$domain
  curl --request PUT --header "PRIVATE-TOKEN: $PAT" --form "certificate=@/etc/letsencrypt/live/$domain/fullchain.pem" --form "key=@/etc/letsencrypt/live/$domain/privkey.pem" https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/pages/domains/www.$domain
else
  echo "Certificate still valid for $days_diff days, no renewal required."
fi