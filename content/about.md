+++
title = "About me"
slug = "about"
+++

Hi, I'm Dylan Drost, an Android fanatic with a passion for creating sleek and beautiful apps that help users accomplish their goals. With over 7 years of experience, I've had the opportunity to work on amazing projects for companies such as BMW, Telegraaf Media Groep, Karify, and SnappCar. Besides Android development, I also dabble in CI/CD and Docker. Currently, I'm working at ING.

## Skills & Expertise
- Android Development
- Kotlin 
- JAVA
- Flutter
- CI/CD
- Docker
- REST APIs