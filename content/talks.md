+++
title = "Talks & Conferences"
slug = "talks"
+++

I have been fortunate to share my knowledge and expertise through presentations and talks at various conferences around the world, including:
- Devfest Amsterdam
- Devfest Switzerland
- Devfest Istanbul
  - [Watch the talk on YouTube](https://www.youtube.com/watch?v=Hc4o2cijW60)
- Devfest Gorky
  - [Watch the talk on YouTube](https://www.youtube.com/watch?v=BWwv5lqDNA8)
- Devfest Coimbra
- AppDevcon
  - [View the session details](https://appdevcon.nl/session/creating-a-flutter-app/)
- Tweakers Dev Summit
- Mobilization
- Droidcon Berlin
  - [Watch the talk on YouTube](https://www.youtube.com/watch?v=sZUbMrKdGcs)