+++
title = "Projects"
slug = "projects"
+++

### ING Nederland
**Android Developer** (August 2020 - December 2022)
- Rebuilt the customer onboarding journey on mobile to scale up and support more products and multiple countries.
- [ING App on Google Play Store](https://play.google.com/store/apps/details?id=com.ing.mobile)

### Royal Schiphol Group
**Android Developer** (December 2018 - July 2020)
- Added new features and implemented a complete redesign of the dashboard for the Schiphol Android app.
- [Schiphol App on Google Play Store](https://play.google.com/store/apps/details?id=org.schiphol)

### SnappCar
**Android Developer** (April 2018 - November 2018)
- Worked on the SnappCar app, porting features from the web platform to the Android app.
- [SnappCar App on Google Play Store](https://play.google.com/store/apps/details?id=nl.snappcar.app)

### Karify & IPPZ
**Android Developer** (October 2015 - March 2018)
- Developed a backend Symfony application creating a REST/HATEOAS API for mobile apps and created the Karify Android application.
- [Karify App on Google Play Store](https://play.google.com/store/apps/details?id=com.karify.android.client)
