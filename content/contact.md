+++ 
title = "Contact" 
slug = "contact" 
+++

Feel free to get in touch with me through any of these channels:
- [LinkedIn](https://www.linkedin.com/in/dylandrost/)
- [GitHub](https://github.com/aegis123)
- [Twitter](https://twitter.com/aegis321)
- [GitLab](https://gitlab.com/dylan.drost)